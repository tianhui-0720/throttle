/**
 * 
 * @param {Function} fn 回调函数
 * @param {Number} delay 延迟时间 默认200
 * @returns 
 */
export function Throttle(fn, delay) {
  let lastTime = null;
  let timer = null;
  const time = delay || 200;
  return function() {
    const args = arguments;
    // 记录当前函数触发的时间
    const nowTime = Date.now();
    if (lastTime && nowTime - lastTime < time) {
      clearTimeout(timer);
      timer = setTimeout(function() {
        // 记录上一次函数触发的时间
        lastTime = nowTime;
        // 修正this指向问题
        fn.apply(this, args);
      }, time);
    } else{
      lastTime = nowTime;
      fn.apply(this, args);
    }
  }
}

export function throttle(fn, delay) {
  let timer = null;
  const time = delay || 200;
  let lastTime = null;

  return function() {
    const args = arguments;
    const nowTime = Date.now();

    if (lastTime && nowTime - lastTime < time) {
      clearTimeout(timer);
      timer = setTimeout(function() {
        lastTime = nowTime;
        fn.apply(this, args);
      }, time)
    } else {
      lastTime = nowTime;
      fn.apply(this, args);
    }
  }
}
